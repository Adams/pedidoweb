import Cookies from 'js-cookie';

export const isLogged = () => {
    let token = Cookies.get('token_idcliente');
    return (token) ? true : false;
}

export const doLogin = (token, rememberPassword = false) => {
    if(rememberPassword) {
        Cookies.set('token_idcliente', token, {expires: 900})
    } else {
        Cookies.set('token_idcliente', token)
    }

}

export const doLogout = () => {
    Cookies.remove('token_idcliente');
}