/* eslint-disable import/no-anonymous-default-export */
import Cookies from "js-cookie";
import qs from 'qs';
import { applyMiddleware } from "redux";


const BASEAPI = 'http://192.168.18.200/PDVServer.dll/datasnap/rest/TSM';

const apiFetchGet = async (endpoint, body = []) =>{    
    if (!body.token) {        
        let token = Cookies.get('token_idcliente');
        if(token) {
            body.token = token;
        }
    }        

//`${BASEAPI+endpoint}/${qs.stringify(body)}`
    let res = await fetch(BASEAPI+endpoint+body);  
    
    // res = res + '["error": "CPF e/ou senha errados!"]';

    const json = await res.json();     
    
    return json;        
}

const apiFetchPut = async (endpoint, body) => {

    console.log(body);
    console.log(BASEAPI+endpoint);    
    const res = await fetch(BASEAPI+endpoint, {
        method:'PUT',
        headers:{                                    
            "Content-Type":"application/x-www-form-urlencoded",
            "Access-Control-Allow-Origin":"*",
            "Accept-Encoding":"identity",                        
            "Accept":"application/json, text/plain; q=0.9, text/html;q=0.8,",                    
            "AcceptCharset":"UTF-8, *;q=0.8",
            "Server":"Microsoft-IIS/10.0",
            "Access-Control-Allow-Methods":"GET, POST, PUT, DELETE, OPTIONS"

        },
        body:JSON.stringify(body)        
    });
    const json = await res.json();

    return json;
}

const apiFetchPost = async (endpoint, body) => {    
    console.log(body);
    console.log(BASEAPI+endpoint);
    
    const res = await fetch(BASEAPI+endpoint, {
        method:'POST',
        headers:{                                    
            "Content-Type":"application/x-www-form-urlencoded",
            "Access-Control-Allow-Origin":"*",
            "Accept-Encoding":"identity",                        
            "Accept":"application/json, text/plain; q=0.9, text/html;q=0.8,",                    
            "AcceptCharset":"UTF-8, *;q=0.8",
            "Server":"Microsoft-IIS/10.0",
            "Access-Control-Allow-Methods":"GET, POST, PUT, DELETE, OPTIONS"

        },
        body:JSON.stringify(body).toUpperCase().replace('[','').replace(']','')      
    });
    const json = await res.json();

    return json;
}

const PdvAPI = {

    login: async (cpf_cnpj, senha) => {
        const json = await apiFetchGet(
            '/Cliente',
            '/cpf_cnpj = ' + cpf_cnpj + ' and senha = ' + senha                  
        );

        if (json.length === 0) {
            const erro = JSON.parse('{"error": "CPF e/ou senha errados!"}');        
            return erro;
        }
        else {
            return json;
        }         
    },

    getGrupos: async () => {
        const json = await apiFetchGet(
            '/Grupo//Grupo'
        );
        
        return json;
    },

    getProdutos: async (grupo) => {
        const json = await apiFetchGet(
            '/Produtos/produto.id_grupo = ' + grupo            
        );
        
        return json;
    },

    getConta: async (id_cliente, atual=true) => {        
        let str = '';
        
        if(atual) {
            str = '/venda/status = 0 and ';
        } else {
            str = '/venda/';
        }
        
        let json = await apiFetchGet(            
            str + 'venda.id_cliente = ' + id_cliente + '/id_venda desc'
        );                                                    
        
        if (json.length === 0) {
            json = JSON.parse('{"error": "Não existe nenhuma conta aberta no seu CPF."}');        
            return json;
        }
        else {
            return json;
        }         
    },

    getPedidos: async (id_venda) => { 
        let json = await apiFetchGet(
           '/pedido/status<>0 and id_venda = ' + id_venda   + '/ID_PEDIDO DESC'
        );                                                    
        return json;      
    },

    getPedidoItens: async (id_pedido) => {                 
        let json = await apiFetchGet(
            '/PedidoItem/id_pedido=' + id_pedido 
        );                                                        
        return json; 
    },    

    getPedidoCriado: async (id_venda) => { 
        const json = await apiFetchGet('/pedido/status=0 and id_venda=' + id_venda );                                  
        return json;        
    },

    getPedidoId: async () => {
        const jsonID = await apiFetchGet('/select/select GEN_ID(GEN_PEDIDO_ID,1) FROM RDB$DATABASE');
        return jsonID;
    },

    adicionarPedido: async (id_venda, id) => {        
        apiFetchPut(
            '/Pedido',
            {"ID_VENDA":id_venda, "ID_PEDIDO": id}
        ); 
        return '';
    },

    atualizarPedido: async (idPedido, idLocal) => {
        apiFetchPost(
            '/Pedido',
            {"ID_PEDIDO":idPedido, "ID_LOCAL_ENTREGA":idLocal}
        );
    },

    adicionarPedidoItem: async (id_produto, idPedido, qtde) => {
        apiFetchPut(
            '/PedidoItem',
            {"ID_PEDIDO":idPedido, "ID_PRODUTO":id_produto, "QTDE":qtde}
        );

        const getPedidoItens = async (id_pedido) => {                 
            let json = await apiFetchGet(
                '/PedidoItem/id_pedido=' + id_pedido 
            );                                                        
            return json; 
        };
        const json = await getPedidoItens(idPedido);
        return json;
    },

    adicionarPedidoStatus: async (idPedido, idStatus) => {
        const json = await apiFetchPut(
            '/PedidoStatus',
            {"ID_PEDIDO":idPedido, "STATUS":idStatus}
        );

        return json;
    },

    getLocalEntrega: async () => {
        const json = await apiFetchGet(
            '/LocalEntrega'
        );

        return json;
    },

    getCliente: async (id_cliente = '') => {
        let str = '';
        
        if(id_cliente === ''){
            str = '/cliente/';
        } else {
            str = '/cliente/id_cliente='+id_cliente
        }
        const json = await apiFetchGet(
            str
        );
        
        return json;
    },

    atualizarCliente: async (cliente) => {
        // let dados = JSON.stringify(cliente);
        // dados = dados.toUpperCase().replace('[','').replace(']','');
        // cliente = JSON.parse(dados);
        delete cliente.ROWID;
        delete cliente.DATA_CRIACAO;
        apiFetchPost(
            '/cliente',
            cliente
        );
    }
};

export default () => PdvAPI;