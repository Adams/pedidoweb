/* eslint-disable import/no-anonymous-default-export */
const initialState = {
    cpf:''
};

export default (state = initialState, action) => {    
    if(action.typy === 'set_email') {
        return {...state, cpf: action.payload.cpf};
    }

    return state;
}