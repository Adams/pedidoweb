import {React} from "react";
import {PageArea} from './styled';
import {PageContainer} from '../../components/MaisComponents';

const Page = () => {
    // const api = useApi();

    // const [Grupos, setGrupos] = useState([]);
    // const [Produtos, setProdutos] = useState([]);
 
    return (
               
            <PageContainer>                            
                    <PageArea>
                        <h2>Sejam bem vindos ao Sistema Web de Pedidos Jango...</h2>
                     </PageArea>            
             </PageContainer>
        
    );
}

export default Page;