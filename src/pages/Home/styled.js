import styled from 'styled-components';

export const SearchArea = styled.div`
    background-color:#DDD;
    border-bottom: 1px solid #CCC;
    padding:20px 0;

    .searchBox {
        // background-color:#9BB83C;
        background-image: linear-gradient(to right, #61acdc, #1d62a2);
        padding:20px 15px;
        border-radius:5px;
        box-shadow:1px 1px 1px 0.3px rbga(0,0,0,0.2);
        display:flex;

        form {
            flex:1;
            display:flex;

            input, select {
                height:40px;
                border:0;
                border-radius:5px;
                outline:0;
                font-size:15px;
                color:#000;
                margin-right:20px;
            }

            input {
                flex:1;
                padding:0 10px;
            }

            select {
                width: 100%;
                             
            }

            button {
                background-color:#49AEEF;
                font-size:15px;
                border:0;
                border-radius:5px;
                padding:20px 15px;
                color:#FFF;
                height:40px;
                padding:0 20px;
                cursor:pointer;
            }            
        }
    }

    .categoryList {
        display:flex;
        flex-wrap:wrap;
        margin-top:10px;
        font-size:10px;
        margin-bottom:10px;

        .categoryItem {
            width:25%;   
            display:flex;
            margin-top:20px;

            img {
                width: 45px;
                height:45px;
                margin-right:10px;
            }

            &:hover {
                color:#999;
            }
        }
    }
`;

export const PageArea = styled.div`
      h2 {
          font-size:20px;
          text-align: center;
      }

      .list {
            display:flex;
            flex-wrap:wrap;
            font-size:10px;            

            .aditem {
                width:100px;                            
            }
      }
      
`;