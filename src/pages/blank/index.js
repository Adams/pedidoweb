import {React, useState, useEffect} from "react";
import { useParams } from "react-router-dom";
import {PageArea, Fake} from './styled';
import {PageContainer, PageTitle, ErrorMessage} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';
import Cookies from "js-cookie";

const Page = () => {
    const api = useApi();
    // const {id} = useParams();    
    const id_cliente = Cookies.get('token_idcliente');

    const [loading, setLoading] = useState(true);
    const [itemInfo, setItemInfo] = useState([]);

    useEffect(() => {
        const getConta = async (id_cliente) => {
            const json = await api.getConta(id_cliente);
        }
        getConta(id_cliente);
    }, [])

    return (
        <PageContainer>
            <PageTitle>
                Modelo
                <PageArea>
                    <div className="leftSide"> 
                        <div className="box"> 
                            <div className="adImage"> 
                                {loading && <Fake height={300}/>} 
                                
                            </div>
                            <div className="adInfo"> 
                                <div className="adName"> 
                                    {loading && <Fake height={20}/>}                                                                        
                                </div>
                                <div className="adDescription"> 
                                    {loading && <Fake height={20}/>}                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="rightSide"> 
                        <div className="box box--padding">
                            {loading && <Fake height={20}/>} 
                        </div>
                        <div className="box box--padding">
                            {loading && <Fake height={50}/>}    
                        </div>
                    </div>
                </PageArea>
            </PageTitle>
        </PageContainer>
    );
}

export default Page;