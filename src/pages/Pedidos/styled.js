import styled from 'styled-components';

export const Fake = styled.div`
    background-color:#DDD;
    height:${props=>props.height || 20}px;
`;

export const PageArea = styled.div`
    display:flex;
    margin-top:5px;

    .box {
        background-color:#FFF;
        border-radius:5px;
        box-shadow:0px 0px 4px #999;
        margin-bottom:20px;                
        font-size:20px;
    }
    .box--padding {
        padding:5px;
        font-size:15px;        
    }

    .rightSide {
        flex:1;
        margin-right: 10px;
        margin-left: 10px;

        .adImage {

        }

        .adInfo {
            padding:10px;
            font-size:20px;

            .adName {
                margin-bottom:20px;
            }

            .adDescription {

            }

        }
    }

    h2 {
        font-size:14px;
        
    }    

    .button {
        border:0;
        background:none;
        color:#FFF;
        font-size:14px;
        text-decoration:none;
        font-weight:bold;
        outline:0;
        position: absolute; 
        

        &:hover {
            color:#999;
        }

        &.button {
            background-color:#FF8100;
            border-radius:4px;
            color:#FFF;
            padding:5px 10px;
        }

        &.button:hover {
            background-color:#E57706; 
        }
    }

    li {
        display:block;
        border:1px solid #FFF;
        margin:5px;        
        text-decoration:none;
        padding:5px;
        border-radius:5px;        
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;
        min-height:30px;
        font-weight:bold;
        width:100%; 
    }
   
`;