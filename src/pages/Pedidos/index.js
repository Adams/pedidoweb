import {React, useState, useEffect} from "react";
// import { useParams } from "react-router-dom";
// import {Slide} from 'react-slideshow-image';
import {PageArea, Fake} from './styled';
import {PageContainer, PageTitle, ErrorMessage} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';
import Cookies from "js-cookie";
import AdPedido from '../../components/partiais/AdPedido';
import {Link} from 'react-router-dom';
import { parseISO, format} from 'date-fns';
import Modal from '../../components/Modal';
import ModalPedidoItens from "../../components/ModalPedidoItens";

const Page = () => {
    const api = useApi();
    // const {id} = useParams();    
    const id_cliente = Cookies.get('token_idcliente');        

    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const [conta, setConta] = useState([]);  
    const [pedidos, setPedidos] = useState([]);       
    const [pedidoItens, setPedidoItens] = useState([]);
    const [id_venda, setIdVenda] = useState([]);
    const [idPedido, setIdPedido] = useState([]);

    const [modalStatus, setModalStatus] = useState(true);

    useEffect(() => {
        // setError('');
        setLoading('true');

        const getConta = async (id_cliente) => {
            // console.log(id_cliente);

            const json = await api.getConta(id_cliente);
            setConta(json);            
            setLoading(false);                        

            if(json[0].id_venda){                
                const jsonPedido = await api.getPedidoCriado(json[0].id_venda);

                if (jsonPedido.length === 0){
                    const id = await api.getPedidoId();
                    api.adicionarPedido(json[0].id_venda, id[0].gen_id)   ;
                    setIdPedido(id[0].gen_id);                    
                } else {                    
                    setIdPedido(jsonPedido[0].id_pedido);                    
                }     
            }

            if(json.error){
                setError(json.error);   
            }                   
        }      

        getConta(id_cliente); 
        
    }, []);                                    

    const clickatulizar = async () => {
        const json = await api.getPedidos(id_venda);
        setPedidos(json);
    }

    const formatDateTime = (dateTime) => {
        const data = parseISO(dateTime);
        const dataformatada = format(data, "dd'/'MM'/'yyyy', às ' HH:mm'hs'");

        return dataformatada;          
    }

    return (
        <PageContainer>
            <PageTitle>                
                Conta 
                <botton className='button' onClick={() => clickatulizar}>Atualizar</botton>
                {error && 
                    <ErrorMessage>{error}</ErrorMessage>
                } 
                {!error && 
                    <PageArea>                                           
                        <div className="rightSide"> 
                            <div className="box box--padding">
                                {loading && <Fake height={20}/>}                                                                        
                                {conta[0] && 
                                <>
                                    <h2>{conta[0].nome}</h2> 
                                    <small>Aberta {formatDateTime(conta[0].data_hora)} </small>                                                                           
                                </>
                                }                                  
                            </div>
                            Pedidos
                            <Link to={`/pedidoadd/${idPedido}`} className="button">Fazer Pedido</Link>                            
                            <div className="box box--padding">
                                {loading && <Fake height={20}/>}   
                                {conta[0] && 
                                <>
                                    <AdPedido pedidos={pedidos} setPedidos={setPedidos} itens={setPedidoItens} itens2={pedidoItens} data={conta[0].id_venda} venda={setIdVenda}></AdPedido>                                     
                                </>
                                }
                            </div>
                            Itens
                            <div className="box box--padding">                                                                
                                {pedidoItens[0] &&
                                <>                                    
                                    {pedidoItens.map((i,k) =>                                           
                                            <li key={k}>{i.qtde} | {i.produto}</li>                                                                                    
                                    )}                    
                                </>
                                }                                                                
                            </div>
                        </div>                        
                    </PageArea>
                    
                }                
            </PageTitle>
            <Modal status={modalStatus} setStatus={setModalStatus}>
                <ModalPedidoItens/>
            </Modal>
        </PageContainer>
    );
}

export default Page;