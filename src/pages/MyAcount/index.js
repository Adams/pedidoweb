import {React, useState, useEffect} from "react";
import {PageArea, Fake} from './styled';
import {PageContainer} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';
import Cookies from "js-cookie";
import { Link } from "react-router-dom";
import AdItem from '../../components/partiais/AdItem';
import { parseISO, format} from 'date-fns';

const Page = () => {
    const api = useApi();
    const id_cliente = Cookies.get('token_idcliente');  

    const [cliente, setCliente] = useState([]);
    const [contas, setContas] = useState([]);    
    const [novaSenha, setNovaSenha] = useState('');
    const [loading, setLoading] = useState(false);
    const [status, setStatus] = useState(["Aberta", "Fechada"]);
 
    useEffect(() =>{
        // setLoading('true');

        const getCliente = async (id_cliente) => {
            const json = await api.getCliente(id_cliente);
            setCliente(json);
        }

        const getConta = async (id_cliente) => {            
            const json = await api.getConta(id_cliente, false);
            setContas(json);   
            // setLoading('false');                                              
        }      

        getConta(id_cliente); 
        getCliente(id_cliente);
    }, []);

    const formatDateTime = (dateTime) => {
        const data = parseISO(dateTime);
        const dataformatada = format(data, "dd'/'MM'/'yyyy', às ' HH:mm'hs'");

        return dataformatada;          
    }

    const clickAlterarSenha = () => {
        cliente[0].senha = novaSenha;
        api.atualizarCliente(cliente);
        Cookies.remove('token_idcliente');
        window.location.href = '/signin';
    }

    return (
        <>                
            <PageContainer>                            
                    <PageArea>
                        <h2>Minha conta</h2>
                        {cliente[0] &&
                            <div className="box">                            
                                <label className="area">
                                    <div className="area--title">Nome:</div>
                                    <div className="area--input">
                                        <input 
                                            type="text" 
                                            value={cliente[0].nome}
                                            disabled={true}
                                        />                                
                                    </div>
                                </label>
                                <label className="area">
                                    <div className="area--title">CPF:</div>
                                    <div className="area--input">
                                        <input 
                                            type="text" 
                                            value={cliente[0].cpf_cnpj}
                                            disabled={true}
                                        />                                
                                    </div>
                                </label>
                                <label className="area">
                                    <div className="area--title">Telefone:</div>
                                    <div className="area--input">
                                        <input 
                                            type="text" 
                                            value={cliente[0].telefone_celular}
                                            disabled={true}
                                        />                                
                                    </div>
                                </label>
                                <label className="area">
                                    <div className="area--title">Nova Senha:</div>
                                    <div className="area--input">
                                        <input 
                                            type="password" 
                                            value={novaSenha}                                            
                                            onChange={e=>setNovaSenha(e.target.value)}
                                        />   
                                        {novaSenha &&
                                            <div className="area--input">
                                                <button onClick={clickAlterarSenha}>Alterar senha</button>
                                            </div>
                                            }                             
                                    </div>
                                </label>
                            </div>
                        }
                        <h2>Contas</h2>
                        <div className="box">                            
                            {/* {loading && <Fake height={20}/>}                                                                         */}
                            {contas[0] && 
                            <>
                                {contas.map((i,k) => 
                                    <div key={k} className="box"> 
                                        <label k={k} className="area">
                                            <div className="area--title">Data e Hora da Abertura:</div>
                                            <div className="area--input">
                                                <input 
                                                    type="text" 
                                                    value={formatDateTime(i.data_hora)}
                                                    disabled={true}
                                                />                                
                                            </div>
                                        </label> 
                                        <label k={k} className="area">
                                            <div className="area--title">Valor:</div>
                                            <div className="area--input">
                                                <input 
                                                    type="text" 
                                                    value={i.total}
                                                    disabled={true}
                                                />                                
                                            </div>
                                        </label>     
                                        <label k={k} className="area">
                                            <div className="area--title">Status:</div>
                                            <div className="area--input">
                                                <input 
                                                    type="text" 
                                                    value={status[i.status]}
                                                    disabled={true}
                                                />                                
                                            </div>
                                        </label>   
                                        {i.status === 0 &&
                                            <button className='button' >Pagar</button>
                                        } 
                                        <hr></hr>       
{/* 
                                        <li key={k}>{formatDateTime(i.data_hora)}</li>                                              
                                        <h4>Valor: {i.total}</h4>                                        
                                        <h4>Status: {status[i.status]}</h4>                                                                                
                                        <hr></hr> */}
                                    </div>  
                                    
                                    
                                )}                                                                                                      
                            </>
                            }                                  
                        </div>
                    </PageArea>            
            </PageContainer>
        </>
    );
}

export default Page;