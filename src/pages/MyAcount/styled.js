import styled from 'styled-components';

export const Fake = styled.div`
    background-color:#DDD;
    height:${props=>props.height || 20}px;
`;

export const PageArea = styled.div`
      h2 {
          font-size:20px;
          text-align: center;
      }

      .lab {
        padding:20px 30px;
      }

      h3 {
        font-size:20px;
        text-align: left;
        margin-bottom:0px;
        margin-left:5px;
      }

      h4 {
        font-size:14px;
        text-align: center;
        margin-top:0px;
        margin-bottom:0px;
        margin-left:5px;
      }

      .list {
            display:flex;
            flex-wrap:wrap;
            font-size:10px;            

            .aditem {
                width:100px;                            
            }
      }

      .box {
        display:block;
        border:1px solid #FFF;
        margin:5px;        
        text-decoration:none;
        padding:5px;
        border-radius:5px;        
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;
        min-height:30px;
        font-weight:bold;
        width:98%; 
        align-items:center;
        

        .button {
            border:0;
            background:none;
            color:#FFF;
            font-size:14px;
            text-decoration:none;
            font-weight:bold;
            outline:0;    
            width:30%;  
            
            
    
            &:hover {
                color:#999;
            }
    
            &.button {
                background-color:#FF8100;
                border-radius:4px;
                color:#FFF;
                padding:5px 10px;
            }
    
            &.button:hover {
                background-color:#E57706; 
            }
        }

        li {
            display:block;
            border:1px solid #FFF;
            margin:0px;        
            text-decoration:none;
            padding:5px;
            border-radius:5px;        
            color:#000;
            background-color:#FFF;
            transition:all ease .2s;
            min-height:30px;
            font-weight:bold;
            width:100%; 
            text-align: center;
            margin-top:0;

            h4 {
                margin-top:0px;
            }
        }

        .area--input{
            flex:1;
            display:flex;
    
            input {
                width:200px;
                font-size:14px;
                padding:10px;
                border:1px solid #DDD;
                border-radius:3px;
                outline:0;
                transition: all ease .4s;
    
                &:focus {
                    border:1px solid #333;
                    color:#333;
                }
            }
    
            button {
                background-color:#0089FF;
                border:0;
                outline:0;
                padding:5px 10px;
                border-radius: 4px;
                color:#FFF;
                font-size:15px;
                cursor:pointer;
    
                &:hover {
                    background-color:#006FCE;
                }
            }
        }
    }

      li {
        display:block;
        border:1px solid #FFF;
        margin:5px;        
        text-decoration:none;
        padding:5px;
        border-radius:5px;        
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;
        min-height:30px;
        font-weight:bold;
        width:100%; 
        text-align: center;
    }

    .area {
        display:flex;
        align-items:center;
        padding:2px;
        // max-width:500px;        
        text-align: center;

        .area--title {
            width:100px;
            text-align:right;
            padding-right:10px;
            font-weight:bold;
            font-size:14px;                
            align-items:center;
            
        }

        .area--input{
            flex:1;

            input {
                width:100%;
                font-size:14px;
                padding:10px;
                border:1px solid #DDD;
                border-radius:3px;
                outline:0;
                transition: all ease .4s;

                &:focus {
                    border:1px solid #333;
                    color:#333;
                }
            }
        }
    }

   
      
`;