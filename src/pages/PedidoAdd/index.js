import {React, useState, useEffect} from "react";
import {PageArea, SearchArea} from './styled';
import {PageContainer, PageTitle} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';
import { Link } from "react-router-dom";
import AdItem from '../../components/partiais/AdItem'
import { useParams } from "react-router-dom";
import ModalPedidoItem from "../../components/ModalPedidoItem";
import Modal from "../../components/Modal";

const Page = () => {
    const api = useApi();
    const {idPedido} = useParams();
    const [Grupos, setGrupos] = useState([]);
    const [Produtos, setProdutos] = useState([]);
    const [pedidoItens, setPedidoItens] = useState([]);
    const [localEnterga, setLocalEnterga] = useState([]);
    const [idLocal, setIdLocal] = useState('');
    const [modalStatus, setModalStatus] = useState(false);
    const [modalPedidoItem, setModalPedidoItem] = useState([]);

    useEffect(() => {        

        const getGrupos = async () => {
            const slist = await api.getGrupos();
            setGrupos(slist);
        }        

        const getPedidoCriado = async () => {                                      

            const json2 = await api.getPedidoItens(idPedido);
            setPedidoItens(json2);                      
        }  

        const getLocalEntrega = async () => {
            const json3 = await api.getLocalEntrega();
            setLocalEnterga(json3);
        }

        getGrupos();  
        getPedidoCriado();   
        getLocalEntrega();
    }, []);

    const getItensInseridos = async () => {        
        const json = await api.getPedidoItens(idPedido);
        setPedidoItens(json);
    }          

    const handleChange = async (e) => {        
        const getProdutos = async () => {
                    const slist = await api.getProdutos(e);
                    setProdutos(slist);
                }
        getProdutos();                
    }       

    const handleEnviarPedido = async (e) => {  
        if (idLocal === '')  {
            alert('Favor selecionar Local de Entrega');
        } else {
        api.atualizarPedido(idPedido, idLocal);
        api.adicionarPedidoStatus(idPedido, '1');                  
        }
    } 

    const handleAtualizarPedido = async (e) => {            
        setIdLocal(e);
    } 

    const handlePedidoItemClick = (data) => {        
        setModalPedidoItem(data);
        setModalStatus(true);
        console.log(modalPedidoItem);
    }

    return (
        <>
            <SearchArea>
                <PageContainer>
                    <div className="searchBox2">
                        {pedidoItens &&
                        <>
                            {pedidoItens[0] && 
                            <>
                                <h1>Pedido</h1>
                                <hr></hr>
                                {pedidoItens.map((i,k) =>                            
                                    <li key={k} onClick={(e) => handlePedidoItemClick(i)}>{i.qtde} | {i.produto}</li>
                                )} 
                                <hr/>
                                <h1>Local de Entrega</h1>
                                <select name="state" onChange={e=>handleAtualizarPedido(e.target.value)}>
                                    <option>Selecione local de entrega</option>
                                    {localEnterga.map((i,k) =>
                                        <option key={k} value={i.id_local_entrega}>{i.local}</option>
                                    )}
                                </select>
                                <br/>
                                {idLocal &&
                                    <Link to='/pedidos' onClick={handleEnviarPedido} className="button">Enviar Pedido</Link>       
                                }                   
                                
                            </>
                            }
                        </>                                             
                        }
                        <Modal status={modalStatus} setStatus={setModalStatus}>
                            <ModalPedidoItem data={modalPedidoItem}/>
                        </Modal>
                    </div>
                    <div className="searchBox"> 
                        <form method="GET" action="/ads">
                            {/* <input type="text" name="q" placeholder="O que você procura?"/> */}
                            <select name="state" onChange={e=>handleChange(e.target.value)}>
                                <option>Selecione grupo</option>
                                {Grupos.map((i,k) =>
                                    <option key={k} value={i.id_grupo}>{i.grupo}</option>
                                )}
                            </select>
                        </form>
                    </div>

                    <div className="categoryList">
                        {/*                         <Link key={k} to="" className="categoryItem">
                                     <img src="https://riqs.com.br/wp-content/uploads/Bebidas.png" alt=""/>
                                    
                                     <img style={{ width: 66, height: 58 }} src={`data:image/bmp;base64,${i.imagem}`}/>
                                     <span>{i.produto}</span>
                                 </Link> */}

                            {/* <button>Pesquisar</button>                            */}
                    </div>
                </PageContainer>
            </SearchArea>        
            <PageContainer>                            
                    <PageArea>
                    <div className="list">
                            {Produtos.map((i,k)=>
                                <AdItem key={k} data={i} idPedido={idPedido} itens={setPedidoItens} getItensInseridos={getItensInseridos}></AdItem>                                
                            )}               
                        </div>
                    </PageArea>            
            </PageContainer>
        </>
    );
}

export default Page;