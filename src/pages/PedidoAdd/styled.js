import styled from 'styled-components';

export const SearchArea = styled.div`
    background-color:#DDD;
    border-bottom: 1px solid #CCC;
    padding:0px 0;
 
    h1 {
        font-size: 18px;
        line-height: 0.7;
        margin-top:0px;
               
    }
    .searchBox2 {        
        background-image: linear-gradient(to right, #61acdc, #1d62a2);
        padding:35px 15px;
        border-radius:5px;
        box-shadow:1px 1px 1px 0.3px rbga(0,0,0,0.2);  
        margin-bottom:1px;
        
        select {
            height:40px;
            width: 100%;
            border:0;
            border-radius:5px;
            outline:0;
            font-size:15px;
            color:#000;
            margin-bottom:10px;
        }
    }
    
    .searchBox {        
        background-image: linear-gradient(to right, #61acdc, #1d62a2);
        padding:10px 15px;
        border-radius:5px;
        box-shadow:1px 1px 1px 0.3px rbga(0,0,0,0.2);
        display:flex;        

        form {
            flex:1;
            display:flex;

            input, select {
                height:40px;
                border:0;
                border-radius:5px;
                outline:0;
                font-size:15px;
                color:#000;
                margin-right:20px;
            }

            input {
                flex:1;
                padding:0 10px;
            }

            select {
                width: 100%;                             
            }

            button {
                background-color:#49AEEF;
                font-size:15px;
                border:0;
                border-radius:5px;
                padding:20px 15px;
                color:#FFF;
                height:40px;
                padding:0 20px;
                cursor:pointer;
            }            
        }
    }

    .categoryList {
        display:flex;
        flex-wrap:wrap;
        margin-top:0px;
        font-size:10px;
        margin-bottom:10px;

        .categoryItem {
            width:25%;   
            display:flex;
            margin-top:20px;

            img {
                width: 45px;
                height:45px;
                margin-right:10px;
            }

            &:hover {
                color:#999;
            }
        }
    }

    .button {
        border:0;
        background:none;
        color:#FFF;
        font-size:14px;
        text-decoration:none;
        font-weight:bold;
        outline:0;
        position: absolute; 
        

        &:hover {
            color:#999;
        }

        &.button {
            background-color:#FF8100;
            border-radius:4px;
            color:#FFF;
            padding:5px 10px;
        }

        &.button:hover {
            background-color:#E57706; 
        }
    }

    li {
        display:block;
        border:1px solid #FFF;
        margin:5px;        
        text-decoration:none;
        padding:5px;
        border-radius:5px;        
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;
        min-height:30px;
        font-weight:bold;
        width:100%; 
    }
`;

export const PageArea = styled.div`
      h2 {
          font-size:20px;
          text-align: center;
      }

      .list {
            display:flex;
            flex-wrap:wrap;
            font-size:10px;            

            .aditem {
                width:100px;                            
            }
      }
      
`;