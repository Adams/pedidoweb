import {React, useState} from "react";
import { useParams } from "react-router-dom";
import {PageArea, Fake} from './styled';
import {PageContainer, PageTitle, ErrorMessage} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';

const Page = () => {
    const api = useApi();
    const{id} = useParams();    

    const [loading, setLoading] = useState(true);
    const [itemInfo, setItemInfo] = useState([]);

    return (
        <PageContainer>
            <PageTitle>
                Item
                <PageArea>
                    <div className="leftSide"> 
                        <div className="box"> 
                            <div className="adImage"> 
                                {loading && <Fake height={300}/>} 
                                
                            </div>
                            <div className="adInfo"> 
                                <div className="adName"> 
                                    {loading && <Fake height={20}/>}                                                                        
                                </div>
                                <div className="adDescription"> 
                                    {loading && <Fake height={20}/>}                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="rightSide"> 
                        <div className="box box--padding">
                            {loading && <Fake height={20}/>} 
                        </div>
                        <div className="box box--padding">
                            {loading && <Fake height={50}/>}    
                        </div>
                    </div>
                </PageArea>
            </PageTitle>
        </PageContainer>
    );
}

export default Page;