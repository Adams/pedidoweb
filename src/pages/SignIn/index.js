import {React, useState} from "react";
import {PageArea} from './styled';
import {PageContainer, PageTitle, ErrorMessage} from '../../components/MaisComponents';
import useApi from '../../helpers/PdvAPI';
import {doLogin} from '../../helpers/AuthHandler';

const Page = () => {
    const api = useApi();

    const [cpf_cnpj, setcpf_cnpj] = useState('');
    const [password, setPassword] = useState('');
    const [rememberPassword, setRememberPassword] = useState('');
    const [disable, setDisable] = useState(false);
    const [error, setError] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        // setDisable(true);
        setError('');

        const json = await api.login(cpf_cnpj, password);
                
        if(json.error){
            setError(json.error);
            // setDisable(false);
        } else {
            doLogin(json[0].id_cliente, rememberPassword);            
            window.location.href = '/';
        }

        // setDisable(false);
    }

    return (
        <PageContainer>
            <PageTitle>
                Login
                <PageArea>
                    {error && 
                        <ErrorMessage>{error}</ErrorMessage>
                    }

                    <form onSubmit={handleSubmit}>
                        <label className="area">
                            <div className="area--title">CPF</div>
                            <div className="area--input">
                                <input 
                                    type="text" 
                                    disabled={disable}
                                    value={cpf_cnpj}
                                    onChange={e=>setcpf_cnpj(e.target.value)}
                                />                                
                            </div>
                        </label>
                        <label className="area">
                            <div className="area--title">Senha</div>
                            <div className="area--input">
                                <input 
                                    type="password" 
                                    disabled={disable}
                                    value={password}
                                    onChange={e=>setPassword(e.target.value)}
                                />
                            </div>
                        </label>
                        <label className="area">
                            <div className="area--title">Lembrar Senha</div>
                            <div className="area--inputbox">
                                <input 
                                    type="checkbox" 
                                    disabled={disable}
                                    value={rememberPassword}
                                    onChange={()=>setRememberPassword(!rememberPassword)}
                                />                                    
                            </div>
                        </label>
                        <label className="area">
                            <div className="area--title"></div>
                            <div className="area--input">
                                <button disabled={disable}>Fazer Login</button>
                            </div>
                        </label>
                    </form>
                </PageArea>
            </PageTitle>
        </PageContainer>
    );
}

export default Page;