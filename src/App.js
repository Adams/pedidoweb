import React from "react";
import { connect } from 'react-redux';

import './App.css';

import { BrowserRouter } from "react-router-dom";
import Routes from './Routes';

import {Template} from './components/MaisComponents'
import Herder from './components/partiais/Header';
import Footer from './components/partiais/Footer';
import Informacoes from './components/partiais/Informacoes';

const Page = (props) => {
    return (
        <BrowserRouter>
            <Template>
                <Herder/>

                <Routes/>

                <Informacoes/>
                <Footer/>                
            </Template>            
        </BrowserRouter>        
    );
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

const mapDispatchToProps = (despatch) => {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(Page);