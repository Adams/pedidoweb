import styled from "styled-components";

export const Item = styled.a`
    a, li {
        display:block;
        border:1px solid #FFF;
        margin:5px;        
        text-decoration:none;
        padding:5px;
        border-radius:5px;        
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;
        min-height:30px;
        font-weight:bold;
        width:98%;                

        &:hover {
            background-color:#EEE;
            border:2px solid #CCC;
        }
    }

    .box {
        background-color:#FFF;
        border-radius:5px;
        box-shadow:0px 0px 4px #999;
        margin-bottom:10px;
        margin-right:10px;
        width:100%;         
        h2 {
            margin-bottom:0px;
            margin-top:0px;
            font-size:14px;
        }
    
        h3 {
            margin-bottom:0px;
            margin-top:0px;
            font-size:14px;
            background-color:#EEE;
        }
    }

    


`;