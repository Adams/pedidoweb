/* eslint-disable import/no-anonymous-default-export */
import {React, useState, useEffect} from "react";
import {Item} from './styled';
import useApi from '../../../helpers/PdvAPI';
import { parseISO, format} from 'date-fns';

export default (props) => {
    const api = useApi();
       
    // const [error, setError] = useState('');
    // const [loading, setLoading] = useState(true);

    useEffect(() => {
        // setError('');
        // setLoading('true');

        const getPedidos = async (id_venda) => {            

            const json = await api.getPedidos(id_venda);
            props.setPedidos(json);            
            // setLoading(false);                        

            // if(json.error){
            //     setError(json.error);   
            // }                     
        }

        getPedidos(props.data);    
        props.venda(props.data);
    }, []); 

    const formatDateTime = (dateTime) => {
        const data = parseISO(dateTime);
        const dataformatada = format(data, "dd'/'MM'/'yyyy', às ' HH:mm'hs'");

        return dataformatada;        
    }  

    const handleClick = async (e) => {        
        const json = await api.getPedidoItens(e);
        props.itens(json);       
    }

    return (
        <Item className="aditem">
            <div>                
                <div className="itemName"></div>
                {props.pedidos &&
                <>
                    {props.pedidos.map((i,k) =>   
                        <div className="box">                            
                            <li key={k} onClick={() => handleClick(i.id_pedido)}>
                                <h2>Pedido: {i.id_pedido}</h2>
                                <h2>Local: {i.local_entrega}</h2>
                                <h2>{formatDateTime(i.data_hora)}</h2>
                                <h3>Status: {i.status_descricao}</h3>                                
                                {/* Pedido {i.id_pedido} | {i.local_entrega}, {formatDateTime(i.data_hora)} | Status:{i.status_descricao} */}
                            </li>
                            {/* {pedidoItens && 
                            <>                            
                                <li key={k} className="box box--padding">                                                                                                    
                                    <>                                    
                                        {pedidoItens.map((i2,k) =>                                                 
                                                <li   li key={k}>{i2.qtde} | {i2.produto}</li>                                                                                    
                                                
                                        )}                    
                                    </>                                                                
                                </li>
                            </>
                            }                              */}
                        </div> 
                    )} 
                </>}                
            </div>
        </Item>
    )
}