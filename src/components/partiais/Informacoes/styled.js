import styled from 'styled-components';

export const FooterArea = styled.div`
    height:80px;
    display:flex;
    justify-content:center;
    align-items:center;
    text-align:center;
    color:#999;
    font-size:14px;
    border-top:1px solid #CCC;
    margin-top:10px;

    .logo {
        display:flex;
        text-align:left;
        
    }
    .contato {
        margin-left:30px;
    }

    img{
        height:60px;    
    }
`;