import React from "react";
import {FooterArea} from './styled';
import {Link} from 'react-router-dom';

const Footer = () => {
    const handleClick = () => {
        window.open('https://tanztecnologia.com.br/', '_blank');                
    }
    return (
       
          
        <FooterArea>
            <div className="logo">                                    
                <div>
                    <img onClick={() => handleClick()} src="https://tanztecnologia.com.br/wp-content/uploads/2021/12/LOGO-OFICIAL2-2048x546.png"></img>
                </div>                    
                <div className="contato">
                    Contatos 
                <br/>
                    Adams (65) 9.99230-9834                
                <br/>
                    Junior  (65) 9.99332-7731
                </div>

            </div>             
        </FooterArea>
    );
}

export default Footer;