import styled from "styled-components";

export const Item = styled.a`
    .box {
        display:block;
        border:1px solid #FFF;
        margin:5px;
        text-decoration:none;
        padding:5px;
        border-radius:5px;
        color:#000;
        background-color:#FFF;
        transition:all ease .2s;                
        min-height:180px;           

        &:hover {
            background-color:#EEE;
            border:2px solid #CCC;
        }

        .itemImage img {
            border-radius:5px;                      
            max-width:100px;            
            max-height:70px; 
            min-height:70px;                   
        }

        .itemName {
            font-weight:bold;            
        }
    }
`;