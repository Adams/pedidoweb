/* eslint-disable import/no-anonymous-default-export */
import React, {useReducer, useEffect, useState} from "react";
import {Link} from 'react-router-dom';
import {Item} from './styled';
import useApi from '../../../helpers/PdvAPI';

export default (props) => {
    const api = useApi();
    const initialState = {
        contagem: 1
    }    

    const reducer = (state, action) => {

        switch(action.type) {
            case 'adicionar':
                return {...state, contagem: state.contagem + 1};
            break;    
            case 'diminuir':
                return {...state, contagem: state.contagem - 1};
            break;    
            case 'dobrar':
                return {...state, contagem: state.contagem * 2};
            break;    
            case 'zerar':
                return {...state, contagem: action.contagem = 1};
            break;  
            
            default: 
        }
    
        return state;
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const handleClick = async () => {                  
        const json = await api.adicionarPedidoItem(props.data.id_produto, props.idPedido, state.contagem);
        state.contagem = 1;  
                      
        props.getItensInseridos(json);        
    }

    return (
        <Item className="aditem">            
            <div className="box"> 
                <div className="itemImage">
                    <img src={`http://192.168.18.200/imagens/${props.data.imagem}`} alt=""/>
                </div>
                <div className="itemName">{props.data.produto}</div>
                <div className="itemPrice">R$ {props.data.preco_venda}</div>
                <div>
                        <button onClick={()=>dispatch({type:'adicionar'})}>+</button>
                        <span>{state.contagem}</span>
                        <button onClick={()=>dispatch({type:'diminuir'})}>-</button>
                        <button onClick={() => handleClick()}>Adicionar</button>                                                            
                </div> 
            </div>
        </Item>
    )
}