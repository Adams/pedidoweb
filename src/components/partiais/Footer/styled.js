import styled from 'styled-components';

export const FooterArea = styled.div`
    height:30px;
    display:flex;
    justify-content:center;
    align-items:center;
    text-align:center;
    // color:#999;
    background-image: linear-gradient(to right, #61acdc, #1d62a2); 
    font-size:14px;
    border-top:1px solid #CCC;
    margin-top:10px;
    font-weight:bold;
    color:#FFF;
`;