import React from "react";
import {FooterArea} from './styled';

const Footer = () => {
    return (
        <FooterArea>
            Todos os direitos reservados a TANZ Tecnologia.             
        </FooterArea>
    );
}

export default Footer;