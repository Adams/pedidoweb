/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import {HeaderArea} from './styled';
import {Link} from 'react-router-dom';
import { isLogged, doLogout } from "../../../helpers/AuthHandler";

const Header = () => {
    let logged = isLogged();

    const handleLogout = () => {
        doLogout();  
        window.location.href = '/signin';
    }

    return (
        <HeaderArea>
            <div className="container">                
                <div className="logo">                    
                    <Link to="/">
                        <img src="http://pesquepaguejango.com.br/site/img/logo.png"></img>
                        {/* <span className="logo-1">T</span>
                        <span className="logo-2">A</span>
                        <span className="logo-3">N</span>
                        <span className="logo-4">Z</span> */}
                    </Link>
                </div>    
                <nav>
                    <ul>
                        {logged && 
                            <>
                                <li>
                                    <Link to="/myacount">Minha Conta</Link>
                                </li>    
                                <li>
                                    <Link to="/signin" onClick={() => handleLogout()}>Sair</Link>
                                </li>    
                                <li>
                                    <Link to="/pedidos" className="button">Pedidos</Link>
                                </li>                                     
                            </>
                        }
                        {!logged &&
                            <>
                                <li>
                                    <Link to="/signin">Login</Link>
                                </li>                      
                            </>
                        }                                                
                    </ul>
                </nav>
            </div>
        </HeaderArea>
    );
}

export default Header;