import styled from 'styled-components';

export const Template = styled.div``;

export const PageContainer = styled.div`
    max-width:1000px;
    margin:auto;
`;

export const PageTitle = styled.h1`
    font-size:27px;    
    border-radius:5px;    
    margin-bottom:20px;
    text-align: center;

    .button {
        border:0;
        background:none;
        color:#FFF;
        font-size:14px;
        text-decoration:none;
        font-weight:bold;
        outline:0;
        position: absolute; 
        

        &:hover {
            color:#999;
        }

        &.button {
            background-color:#FF8100;
            border-radius:4px;
            color:#FFF;
            padding:5px 10px;
        }

        &.button:hover {
            background-color:#E57706; 
        }
    }
`;

export const PageBody = styled.div``;

export const ErrorMessage = styled.div`
    margin:10px 0;
    background-color:#FFCACA;
    color:#000;
    border:2px solid #FF0000;
    padding:5px;
    font-size:15px;
`;
