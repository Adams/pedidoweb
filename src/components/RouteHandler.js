/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import {Route} from 'react-router-dom';
import {isLogged} from '../helpers/AuthHandler';
import {Navigate} from "react-router-dom"; 

export default ({children, ...rest}) => {
    let logged = isLogged(); 
    console.log(isLogged)  ;

    if (!logged) {
        return <Navigate to='/signin'/>;
    }
    
    return children;
    
}