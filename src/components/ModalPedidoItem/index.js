/* eslint-disable import/no-anonymous-default-export */
import React from "react";
import {ProductPrice, ProductQuantity, ProductQtdeImage, ProductQtdeText, ProductButton, ProductName, Conteiner, ProductQuantityArea, ProductArea, ProductPhoto, ProductInfoArea, ProductDetails, ProductButtons} from './styled';

export default (data) => {
console.log(data);

    return (
        <Conteiner>
            <ProductArea>
                <ProductPhoto src={`http://192.168.18.200/imagens/${data.data.id_produto}.png`} />
                <ProductInfoArea>
                    <ProductDetails>
                        <ProductName>{data.data.produto}</ProductName>                        
                    </ProductDetails>
                    <ProductQuantityArea>
                        <ProductQuantity>
                            <ProductQtdeImage src="/mais.png"/>
                            <ProductQtdeText>{data.data.qtde}</ProductQtdeText>                        
                            <ProductQtdeImage src=""/>
                        </ProductQuantity>
                        <ProductPrice>
                            R$
                        </ProductPrice>
                    </ProductQuantityArea>
                </ProductInfoArea>                
            </ProductArea>
            <ProductButtons>
                <ProductButton small={true}>Cancelar</ProductButton>
                <ProductButton>Alterar</ProductButton>
            </ProductButtons>
        </Conteiner>
    )
}