import styled from 'styled-components';

export const Conteiner = styled.div`
    width: 340px;
    padding:10px;
`;

export const ProductArea = styled.div`
    height:130px;
    // background-color:#FF0000;
    display: flex;
`;

export const ProductPhoto = styled.img`
    max-width: 120px;  
    border-radius:20px;  
`;

export const ProductInfoArea = styled.div`
    flex:1;
    // background-color:#0000FF;
    display:flex;
    flex-direction:column;
    justify-content: space-between;
    margin-left:10px;
`;

export const ProductDetails = styled.div`
    height: 45px;
    // background-color: #FF0000;
`;

export const ProductButtons = styled.div`
    height: 40px;
    // background-color:#00FF00;
    display:flex;
    justify-content:flex-end;
    align-items:center;
`;

export const ProductName = styled.div`
    font-size: 14px;
    font-weight:bold;
`;

export const ProductButton = styled.button`
    border:0;
    background-color:#FF8100;
    box-shadow:2px 2px 0px #333;
    color:#FFF;
    font-size:${props=>props.small ? '12px' : '16px'};
    font-weight: bold;
    padding:${props=>props.small ? '5px 10px' : '10px 20px'};
    margin-left:10px;
    border-radius:5px;
`;

export const ProductQuantityArea = styled.div`
    height:25px;
    display:flex;
    justify-content:space-between;
`;

export const ProductQtdeImage = styled.div`
    width:24px;
    height:auto;
`;

export const ProductQtdeText = styled.div`
    font-size:16px;
    font-weight:bold;
`;

export const ProductQuantity = styled.div`
    height: 45px;
    // background-color: #FF0000;
`;

export const ProductPrice = styled.div`
    height: 30px;
    font-weight:bold;
    font-size:16px;
`;