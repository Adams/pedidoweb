/* eslint-disable import/no-anonymous-default-export */
import React, {useReducer, useEffect, useState} from "react";
import {Conteiner, BodyConteiner} from './styled';

export default ({status, setStatus, children}) => {    
    const handleModalClick = (e) => {
        if(e.target.classList.contains('modalBg')) {
            setStatus(false);
        }
    }


    return (
        <Conteiner className="modalBg" status={status} onClick={handleModalClick}>            
            <BodyConteiner>
                {children}
            </BodyConteiner>
        </Conteiner>
    )
}