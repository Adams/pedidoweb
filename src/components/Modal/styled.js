import styled from "styled-components";

export const Conteiner = styled.div`   
    display:${props=>props.status ? 'flex' : 'none'};
    position: fixed;
    left:0;
    top:0;
    right:0;
    bottom:0;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 900;    
    justify-content: center;
    align-items: center;
`;

export const BodyConteiner = styled.div`   
    background-color: #FFF;
    border-radius: 20px;
    box-shadow: 0px 0px 80px #FF8100;
    max-height: 85vh;
    max-width: 85vw;
    overflow:auto;
`;