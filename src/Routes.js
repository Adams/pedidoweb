import React from "react";
import {Routes, Route} from 'react-router-dom';
import RouteHandler from './components/RouteHandler';

import Home from './pages/Home';
import About from './pages/About';
import NotFound from './pages/NotFound/index';
import SingIn from './pages/SignIn';
// import AdPage from './pages/blank';
import Pedidos from './pages/Pedidos';
import PedidoAdd from './pages/PedidoAdd';
import MyAcount from './pages/MyAcount';

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
    return (
        <Routes>            
            <Route path="/about" element={<About />}/>                            
            <Route path="/signin" element={<SingIn />}/>                        
            <Route path="/pedidos" element={<RouteHandler> <Pedidos/> </RouteHandler>}/>                            
            <Route path="/pedidoadd/:idPedido" element={<RouteHandler> <PedidoAdd/> </RouteHandler>}/>                            
            <Route path="/myacount" element={<RouteHandler> <MyAcount/> </RouteHandler>}/>                            
            <Route path="/" element={<Home />}/>                
            <Route path="*" element={<NotFound />}/>                
        </Routes>
    );
}